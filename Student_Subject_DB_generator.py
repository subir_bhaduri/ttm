# Required libraries are imported
import os
# import tkinter.ttk as ttk

import openpyxl
from openpyxl.utils.cell import coordinate_from_string, column_index_from_string, get_column_letter
import pandas as pd
import numpy as np
import itertools
import tkinter as Tk
from tkinter import filedialog
from tkinter import messagebox
from openpyxl.styles import Alignment, Border, Color, Font, PatternFill, Side
import time
from openpyxl.utils.dataframe import dataframe_to_rows

print(pd.__version__)

"""
	LIST OF CONSTRAINTS
	1. No faculty should be allotted two or more classes in the same time slot.
	2. No student should be allotted two or more classes in the same time slot.
	3. No faculty should be allotted the morning 7:30 AM class and the afternoon 5:00 PM class, both together.
	4. No student should be allotted consecutive classes on all time slots. 
"""


class ProgramContainer:
	def __init__(self):
		# All the required variables are declared.

		self.box2_TT = None
		self.box1_records = None

		self.root = Tk.Tk()
		self.window1 = Tk.Frame(self.root)
		self.theme_select = None

		self.prn_row = None
		self.check_prn = None
		self.col_l = None
		self.col_r = None

		self.elective_course_name = ""
		self.elective_faculty_name = ""

		self.save_files_dir = None
		self.root_filename = ""

		self.i = 0
		self.j = 0

		self.corner_reference = None

		self.TT_machine_file1 = 'Timetable Slots Template1.xlsx'
		self.TT_machine_file2 = 'Timetable Slots Template2.xlsx'

		self.max_cols = 0
		self.max_rows = 0

		self.collision_count_Student_Course = 0
		self.collision_count_Course = 0
		self.collision_count_Faculty = 0
		self.collision_count_Student = 0
		self.temp_list_count = []

		self.collision_index_Course = []
		self.collision_index_Faculty = []
		self.collision_index_Student = []
		self.collision_index_Student_Course = []

		self.all_possible_groupings = []

		self.weekdays = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']
		self.time_slots = ['7:30 am – 9:30 am', '9:45 am – 11:45 am', '12:30 pm – 2:30 pm',
						   '2:45 pm – 4:45 pm', '5:00 pm – 7:00 pm']

		self.database_df = None
		self.elective_records_df = None
		self.essential_records_df = None
		self.electives_df = None
		self.courses_df = None
		self.faculty_table_df = None
		self.course_table_df = None
		self.all_groupings_df = None
		self.ranked_groupings_df = None

		self.electives_student_name = []
		self.electives_student_email = []
		self.electives_student_prn = []
		self.electives_student_courses = []
		self.electives_faculty = []
		self.electives_batch_year = []

		self.all_student_emails = []
		self.all_student_names = []
		self.all_faculty = []
		self.all_courses = []
		self.all_paper_names = []
		self.all_student_PRN = []

		self.every_PRN_year_data = []

		self.elective_locations = []
		self.course_locations = []

		self.row_num = None

		self.obtain_batch_year = []
		self.obtain_faculty_name = []
		self.obtain_course_name = []
		self.obtain_paper_name = []
		
		self.obtain_elective_course_name = []
		self.obtain_elective_faculty_name = []

		self.wb1 = None
		self.wb1_sheet_names = None

		self.col_faculty = ''
		self.col_course = ''
		self.col_paper_name = ''

		self.faculty1 = None

		self.output_all_machine_files = 0
		self.program_time_duration = 0

	# Function Description: Act similar to the main function, by controlling the flow of program.
	def Program_Control(self):
		self.Program_GUI()

	def Excel_File_Processing(self):
		print("jnbjfnsd")
		time_duration_start = time.time()
		self.Variables_Initialise()
		self.Pull_Data_from_Excel_File()
		self.Manage_Dataframes()
		
		if (self.output_all_machine_files == 1) or (self.output_all_machine_files == 0):
			self.Essential_TT_Components()
		
		
		
		self.Student_Records_Classification()
		print("done")
		'''
		time_duration_end = time.time()
		self.program_time_duration = int(time_duration_end - time_duration_start)
		self.GUI_Collision_Results()
		'''

	# Function Description: Initialise some variables for use in functions.
	def Variables_Initialise(self):
		# print('variables initialising')
		"""
		file_name : The Excel file provided by Admin Dept, containing multiple sheets,
		with data of multiple courses and batches.
		"""
		# file_name = "Student's course Data January to May 2022.xlsx"
		file_name = self.root_filename
		print(file_name)
		self.wb1 = openpyxl.load_workbook(filename=file_name)
		self.wb1_sheet_names = self.wb1.sheetnames
		self.row_num = 2

	def Pull_Data_from_Excel_File(self):
		# Function Description : Reading the Excel file, obtaining the required data.
		# NOTE : First sheet index number is 0
		# Scanning every sheet present in the Excel file.nt
		self.prn_count=0
		self.sub_count=0
		print('pulling data from sheet')
		for sheet_index in range(0, len(self.wb1.sheetnames)):
			batch_year = 0
			course_type = ''
			paper_number = ''	# classification text for courses, like minor, major, paper, etc.
			
			# Reading the contents of sheet names for checking.
			string_check = self.wb1_sheet_names[sheet_index]
			string_check = string_check.lower()
			string_check = string_check.split(sep=None)
			
			print("String_check:",string_check)

			# Storing the batch year.
			if '2026' in string_check:
				batch_year = '2026'
			if '2025' in string_check:
				batch_year = '2025'
			if '2024' in string_check:
				batch_year = '2024'
			if '2023' in string_check:
				batch_year = '2023'
			if '2022' in string_check:
				batch_year = '2022'

			# Storing the type of course.
			if ('elective' in string_check or 'electives' in string_check) and 'honours' not in string_check:
				course_type = 'elective'
			if 'honours' in string_check:
				course_type = 'honours elective'
			if 'extra' in string_check or 'audit' in string_check:
				course_type = 'extra and audit'

			wb1_sheet_obj = self.wb1[self.wb1_sheet_names[sheet_index]]
			total_rows = wb1_sheet_obj.max_row
			total_columns = wb1_sheet_obj.max_column
			wb1_sheet_obj.title = "data pull"
			print("Total_rows:",total_rows)

			# corner_reference stores the cell coordinates.
			self.corner_reference = ''
			
			# NOTE : the upper limit number in for loop is not included
			# if statement is used, to execute loop only when sheet's name contains batch year.
			if batch_year == '2022' or batch_year == '2023' or batch_year == '2024' or batch_year == '2025' or batch_year == '2026':
				for row_iter in range(1, total_rows):
					for column in range(1, total_columns):
						cell_obj = wb1_sheet_obj.cell(row=row_iter, column=column)
						# conversion of string_check to str is prerequisite for split.
						string_check = str(cell_obj.value)
						string_check = string_check.split(sep=None)
						#print("string_check2:",string_check)
						"""
						NOTE : The if-statement below is crucial.
						PRN is the single common ubiquitous column.
						Therefore it acts as anchor for scanning data.
						"""
						if 'PRN' in string_check:
							self.sub_count = self.sub_count +1
							
							# In sheets containing batch_year,
							# paper_number is 3 rows above PRN
							cell_obj = wb1_sheet_obj.cell(row=row_iter - 3, column=column)
							col_paper_name = str(cell_obj.value)
							
							# course name is two rows above PRN.
							cell_obj = wb1_sheet_obj.cell(row=row_iter - 2, column=column)
							col_course = str(cell_obj.value)
							
							# faculty name is one row above PRN.
							cell_obj = wb1_sheet_obj.cell(row=row_iter - 1, column=column)
							col_faculty = str(cell_obj.value)
							
							#corner_reference = cell_obj.coordinate
							#self.course_locations.append([sheet_index, corner_reference])
							
							#print(string_check, col_faculty , col_course)
							
							if (col_paper_name == 'None' and col_course == 'None' and col_faculty == 'None') or (col_faculty == 'Research Cell'):
								# Purpose of this statement is to avoid the above specific edge case.
								pass
							else:
								self.obtain_paper_name.append(col_paper_name)
								self.obtain_course_name.append(self.string_validate(col_course))
								self.obtain_faculty_name.append(self.string_validate(col_faculty))
								self.obtain_batch_year.append(batch_year)
								prn_col = row_iter + 1
								self.row_num += 1
								
								#print(self.obtain_course_name,self.obtain_faculty_name,self.obtain_batch_year)
								
								for iter_v in range(1, 200):
									cell_obj = wb1_sheet_obj.cell(row=prn_col, column=column)
									prn = str(cell_obj.value)
									#print(prn)
									col_name = wb1_sheet_obj.cell(row=prn_col, column=column + 1).value
									col_email = wb1_sheet_obj.cell(row=prn_col, column=column + 2).value
									if prn == 'None':
										pass
									else:
										self.prn_count = self.prn_count + 1
										self.all_student_PRN.append(int(float(prn)))
										self.all_paper_names.append(self.string_validate(col_paper_name))
										self.all_courses.append(self.string_validate(col_course))
										self.all_faculty.append(self.string_validate(col_faculty))
										self.every_PRN_year_data.append(batch_year)
										self.all_student_names.append(col_name)
										self.all_student_emails.append(col_email)
										prn_col += 1
										# misc : wb2_ws.cell(row=row, column=1).value = 'PRN'
				
										#print(self.all_student_names)
			print("prn count:",self.prn_count)
			print("Sub count:",self.sub_count)			

			# The if statement exists to address the different design of sheets containing electives.
			if course_type == 'elective':
				# Here scanning columns takes precedence over rows.
				for column in range(1, total_columns):
					for row_iter in range(1, total_rows):
						cell_obj = wb1_sheet_obj.cell(row=row_iter, column=column)
						string_check = str(cell_obj.value)
						string_check = string_check.split(sep=None)
						# NOTE : Below contains potentially problematic code.
						if 'PRN' in string_check:
							self.col_r = str(wb1_sheet_obj.cell(row=row_iter, column=column + 1)).strip().lower()
							self.col_l = str(wb1_sheet_obj.cell(row=row_iter, column=column - 1)).strip().lower()

							name_col = column - 1
							email_col = column + 1
							corner_reference = cell_obj.coordinate
							# Used below are some very helpful functions from 'openpyxl.utils.cell' library.
							xy = coordinate_from_string(corner_reference)
							column_ind = column_index_from_string(xy[0])
							row_ind = xy[1]
							"""
							NOTE : Course name and faculty name exist in same cell,
							one row above, and two columns leftwards from 'PRN'.
							"""
							row_ind -= 1
							column_ind -= 2
							cell_obj = wb1_sheet_obj.cell(row=row_ind, column=column_ind)
							string_check = cell_obj.value
							corner_reference = cell_obj.coordinate
							self.elective_locations.append([sheet_index + 1, corner_reference])
							self.row_num += 1

							if '-' in string_check.rpartition('-') and ',' not in string_check.rpartition('—'):
								delimiter_char = "-"
							else:
								delimiter_char = ","
							if '-' in string_check.rpartition('-') and ',' in string_check.rpartition(','):
								delimiter_char = ","

							elective_course_name = string_check.rpartition(delimiter_char)[0]
							elective_faculty_name = string_check.rpartition(delimiter_char)[2]
							self.obtain_elective_course_name.append(self.string_validate(elective_course_name))
							self.obtain_elective_faculty_name.append(self.string_validate(elective_faculty_name))

							self.prn_row = row_iter + 1
							while self.prn_row != 0:
								cell_obj = wb1_sheet_obj.cell(row=self.prn_row, column=column)
								if cell_obj.value is None:
									self.prn_row = 0
									break
								else:
									if len(str(cell_obj.value)) < 11:
										self.prn_row += 1
										pass
									else:
										self.electives_student_prn.append(cell_obj.value)
										self.electives_batch_year.append(self.get_batch_year(cell_obj.value))
										s_name = wb1_sheet_obj.cell(row=self.prn_row, column=name_col)
										self.electives_student_name.append(s_name.value)
										s_email = wb1_sheet_obj.cell(row=self.prn_row, column=email_col)
										self.electives_student_email.append(s_email.value)
										self.electives_student_courses.append(
											self.string_validate(elective_course_name))
										self.electives_faculty.append(self.string_validate(elective_faculty_name))
										self.prn_row += 1

			if (course_type == 'honours elective') or (course_type == 'extra and audit'):
				# Here scanning columns takes precedence over rows.
				name_col_delta = 0
				email_col_delta = 0
				for column in range(1, total_columns):
					for row_iter in range(1, total_rows):
						cell_obj = wb1_sheet_obj.cell(row=row_iter, column=column)
						string_check = str(cell_obj.value)
						string_check = string_check.split(sep=None)
						# NOTE : Below contains potentially problematic code.
						if 'PRN' in string_check:
							self.col_r = str(wb1_sheet_obj.cell(row=row_iter, column=column + 1)).strip().lower()
							self.col_l = str(wb1_sheet_obj.cell(row=row_iter, column=column - 1)).strip().lower()

							if course_type == 'honours elective':
								name_col_delta = 1
								email_col_delta = 2
								string_check = wb1_sheet_obj.cell(row=row_iter, column=column + 3).value
								if '-' in string_check.rpartition('-') and ',' not in string_check.rpartition('—'):
									delimiter_char = "-"
								else:
									delimiter_char = ","
								if '-' in string_check.rpartition('-') and ',' in string_check.rpartition(','):
									delimiter_char = ","

								self.elective_course_name = string_check.rpartition(delimiter_char)[0]
								self.elective_faculty_name = string_check.rpartition(delimiter_char)[2]
							else:
								if course_type == 'extra and audit':
									name_col_delta = -1
									email_col_delta = 2

							name_col = column + name_col_delta
							email_col = column + email_col_delta
							self.prn_row = row_iter + 1
							while self.prn_row != 0:
								cell_ob = wb1_sheet_obj.cell(row=self.prn_row, column=column)
								if cell_ob.value is None:
									self.prn_row = 0
									break
								else:
									if len(str(cell_ob.value)) < 11:
										self.prn_row += 1
										pass
									else:
										self.electives_student_prn.append(cell_ob.value)
										self.electives_batch_year.append(self.get_batch_year(cell_ob.value))
										s_name = wb1_sheet_obj.cell(row=self.prn_row, column=name_col)
										self.electives_student_name.append(s_name.value)
										s_email = wb1_sheet_obj.cell(row=self.prn_row, column=email_col)
										self.electives_student_email.append(s_email.value)
										e_course = wb1_sheet_obj.cell(row=self.prn_row, column=column + 3).value
										self.electives_student_courses.append(self.string_validate(e_course))
										self.prn_row += 1

							corner_reference = cell_obj.coordinate
							# Used below are some very helpful functions from 'openpyxl.utils.cell' library.
							xy = coordinate_from_string(corner_reference)
							column_ind = column_index_from_string(xy[0])
							row_ind = xy[1]
							"""
							NOTE : Course name and faculty name exist in same cell,
							one row above, and two columns leftwards from 'PRN'.
							"""
							cell_obj = wb1_sheet_obj.cell(row=row_ind, column=column_ind)
							corner_reference = cell_obj.coordinate
							self.elective_locations.append([sheet_index + 1, corner_reference])
							self.row_num += 1
							# print(elective_locations)

	def Manage_Dataframes(self):
		courses_details = {
			'Batch': np.array(self.obtain_batch_year),
			'Course': np.array(self.obtain_course_name),
			'Paper_name': np.array(self.obtain_paper_name),
			'Faculty': np.array(self.obtain_faculty_name)
		}
		self.courses_df = pd.DataFrame.from_dict(courses_details, orient='index').transpose()

		electives_details = {
			'Elective': np.array(self.obtain_elective_course_name),
			'Faculty': np.array(self.obtain_elective_faculty_name)
		}
		self.electives_df = pd.DataFrame.from_dict(electives_details, orient='index').transpose()

		# All essential data for processing the timetable constraints - includes PRN, Course, and Faculty.
		essential_records = {
			'PRN': np.array(self.all_student_PRN),
			'Batch': np.array(self.every_PRN_year_data),
			'Course': np.array(self.all_courses),
			'Paper Name': np.array(self.all_paper_names),
			'Faculty': np.array(self.all_faculty)
		}
		self.essential_records_df = pd.DataFrame.from_dict(essential_records, orient='index').transpose()

		electives_details = {
			'PRN': np.array(self.electives_student_prn),
			'Name': np.array(self.electives_student_name),
			'Email-ID': np.array(self.electives_student_email),
			'Batch': np.array(self.electives_batch_year),
			'Course': np.array(self.electives_student_courses),
			'Faculty': np.array(self.electives_faculty)
		}
		self.elective_records_df = pd.DataFrame.from_dict(electives_details, orient='index').transpose()

		complete_records = {
			'PRN': np.array(self.all_student_PRN),
			'Name': np.array(self.all_student_names),
			'Email-ID': np.array(self.all_student_emails),
			'Batch': np.array(self.every_PRN_year_data),
			'Course': np.array(self.all_courses),
			'Faculty': np.array(self.all_faculty)
		}
		self.database_df = pd.DataFrame.from_dict(complete_records, orient='index').transpose()
		self.database_df.sort_values(by=['PRN'], ascending=True, inplace=True, ignore_index=True)

		self.elective_records_df.sort_values(by=['PRN'], ascending=True, inplace=True, ignore_index=True)

		if self.box1_records.get() == 1:
			self.Generate_Student_Records()

	def Essential_TT_Components(self):
		self.courses_df.to_csv('courses_data.csv', index=False)
		self.electives_df.to_csv('electives_data.csv', index=False)
		self.essential_records_df.to_csv('essential_data_for_TT.csv', index=False)
		self.elective_records_df.to_csv('electives_data_of_students.csv', index=False)

	def Generate_Student_Records(self):
		mega_database_df = pd.concat(objs=[self.database_df, self.elective_records_df], ignore_index=True)
		mega_database_df.sort_values(by=['PRN'], ascending=True, inplace=True, ignore_index=True)
		mega_database_df.to_csv('mega_database.csv', index=False)

	"""
	The function below is responsible for the following,
	1. create a matrix with x-axis as courses, and y-axis as PRNs
	2. identify students with their courses, and map the data onto the matrix
	3. sort the matrix according to number of students per course in descending order
	4. for an arbitrary course, identify all possible other courses than can take place during same time-slot
	5. output the information collected to the user
	"""

	def CollisionDetect_faculty(self, input_list):
		cdf_mr, cdf_mc = self.courses_df.shape
		for e1 in input_list:
			faculty1 = ''
			course1 = e1
			ns_c1 = 0
			found_c1 = 0
			for e2 in input_list:
				if e2 != e1:
					faculty2 = ''
					course2 = e2
					ns_c2 = 0
					found_c2 = 0
					for m in range(1, cdf_mr):
						if course1 == self.courses_df.iloc[m, 1]:
							ns_c1 += 1
							if found_c1 == 0:
								faculty1 = self.courses_df.iloc[m, 2]
						if course2 == self.courses_df.iloc[m, 1]:
							ns_c2 += 1
							if found_c2 == 0:
								faculty2 = self.courses_df.iloc[m, 2]
					if (faculty1 == faculty2) and (faculty2 != ''):
						if ns_c1 > ns_c2:
							input_list.remove(course2)
						else:
							input_list.remove(course1)
		return input_list

	def Student_Records_Classification(self):
		t_list1 = []
		unique_prn = np.unique(np.array(self.all_student_PRN))
		# Create a matrix with PRN, followed by zeroes along the length of courses.
		for z in range(0, len(unique_prn)):
			t_list2 = [unique_prn[z]]
			for c in range(0, len(self.obtain_course_name)):
				t_list2.append(0)
			t_list1.append(t_list2)

		matrix_df = pd.DataFrame(t_list1)

		# If student has that particular course, then cell content is 1.
		matrix_rows, matrix_cols = matrix_df.shape
		db_rows, db_cols = self.database_df.shape
		for s1 in range(0, matrix_rows):
			prn1 = matrix_df.iloc[s1, 0]
			for s2 in range(0, db_rows):
				prn2 = self.database_df['PRN'].loc[s2]
				if prn1 == prn2:
					course2 = self.database_df['Course'].loc[s2]
					for c in range(0, len(self.obtain_course_name)):
						if course2 == self.obtain_course_name[c]:
							matrix_df.iloc[s1, c + 1] = 1

		# The below number is for 'Total'. It exists to facilitate sorting without an error.
		# Reason: sorting is not possible when a row has string along with integers.
		row_sum = [1010101010101]

		# Count the total number of students for each course.
		for c in range(1, matrix_cols):
			total_students = 0
			for r in range(0, matrix_rows):
				if matrix_df.iloc[r, c] == 1:
					total_students += 1
			row_sum.append(total_students)

		# Total number of students inserted as a new row at the end of the dataframe.
		matrix_df.loc[len(matrix_df)] = row_sum

		# Header row for dataframe containing the names of courses.
		first_row = ['PRN']
		for c in range(0, len(self.obtain_course_name)):
			first_row.append(self.obtain_course_name[c])
		first_r_df = pd.DataFrame(first_row).transpose()
		matrix_df_f = pd.concat([first_r_df, matrix_df], axis=0, ignore_index=True)
		
		# Original place for enabling student-subject matrix
		#matrix_df_f.reset_index(drop=True)
		#matrix_df_f.to_csv('matrix10classify0.csv')

		# Sorting in descending order as per the total number of students.
		sorted_df = matrix_df_f.sort_values(by=(len(unique_prn) + 1), axis=1, ascending=False)

		# Name of method followed for the list below is called List Comprehension.
		col_sort_list = [n for n in range(1, len(matrix_df.columns))]

		# Sorting the columns in descending order so that the ones appear above zeroes.
		sorted_df.sort_values(by=col_sort_list, ascending=False, inplace=True)
		sorted_df.to_csv('matrix10classify1.csv')
		
		'''
		gc_faculty_collisions = []

		---------------------------------------------------------------------------
		# The following code is responsible for grouping of collision-free courses.
		---------------------------------------------------------------------------

		# Going through all courses, starting from the highest to the lowest number of students.
		for c1 in range(1, matrix_cols):
			# backtrack_courses is used to ensure that once a course is grouped, it is not repeated.

			# adding the very first course that is being compared to the group.
			courses_grouped1 = [sorted_df.iloc[0, c1]]
			rows_check1 = []
			match_correct = 0

			# obtaining all locations of '1' in that column.
			for r1 in range(1, matrix_rows):
				if sorted_df.iloc[r1, c1] == 1:
					rows_check1.append(r1)

			# checking for instances of '1' along the specific rows.
			for c2 in range(c1 + 1, matrix_cols):
				for r2 in range(0, len(rows_check1)):
					if sorted_df.iloc[rows_check1[r2], c2] == 1:
						match_correct = 0
						# if row contains a second instance of 1, then break immediately.
						break
					else:
						match_correct += 1

				if match_correct == len(rows_check1):
					courses_grouped1.append(sorted_df.iloc[0, c2])

			courses_grouped2 = courses_grouped1

			# Within the group, checking for collision starting with second course.
			# loop starts from 1 because the first course being compared is at 0.
			for s in range(1, len(courses_grouped1)):
				col_check2 = 0
				# identifying the column containing course that has to now be compared with.
				for mc in range(c1 + 1, matrix_cols):
					if courses_grouped1[s] == sorted_df.iloc[0, mc]:
						col_check2 = mc
						break

				# this if statement exists to prevent an edge-case error of course not being identified
				if col_check2 != 0:
					rows_check2 = []
					# the location of all '1' in that column is now stored into a new list
					for r3 in range(1, matrix_rows):
						if sorted_df.iloc[r3, col_check2] == 1:
							rows_check2.append(r3)
					# print(rows_check2)

					# Checking for instances of '1' in the
					for c3 in range(col_check2 + 1, matrix_cols):
						if (len(rows_check2) < sorted_df.iloc[matrix_rows - 1, c3]) and (
								sorted_df.iloc[0, c3] in courses_grouped2):
							for r4 in rows_check2:
								if sorted_df.iloc[rows_check2[r4], c3] == 1:
									courses_grouped2.remove(sorted_df.iloc[0, c3])
									break
				else:
					pass
			self.all_possible_groupings.append(self.CollisionDetect_faculty(courses_grouped2))

			# print(len(courses_grouped), len(courses_grouped2), len(backtrack_courses))
		# print(all_possible_groupings)

		faculty_collisions_df = pd.DataFrame(gc_faculty_collisions)

		self.all_groupings_df = pd.DataFrame(self.all_possible_groupings)
		duplicate_groupings_df = self.all_groupings_df

		mxr, mxc = duplicate_groupings_df.shape
		total_number_listed = []
		for row in range(0, mxr):
			course_count = 0
			for col in range(0, mxc):
				if duplicate_groupings_df.iloc[row, col] is not None:
					course_count += 1
			total_number_listed.append(course_count)

		temp_df1 = pd.DataFrame(total_number_listed)
		temp_df2 = pd.concat([temp_df1, duplicate_groupings_df], axis=1, ignore_index=True)
		header_col_names = [1010101010101]
		for n in range(0, mxc):
			col_name = 'Col' + str(n+1)
			header_col_names.append(col_name)
		header_df = pd.DataFrame(header_col_names).transpose()
		self.ranked_groupings_df = pd.concat([header_df, temp_df2], axis=0, ignore_index=True).reset_index()
		all_possible_groupings_ranked = self.ranked_groupings_df.sort_values(by=[0], axis=0, ascending=False)

		self.ranked_groupings_df.sort_values(by=[0], axis=0, ascending=False, inplace=True)
		rk_mxr, rk_mxc = self.ranked_groupings_df.shape
		backtrack_courses = []
		for row in range(0, rk_mxr):
			for col in range(0, rk_mxc):
				if len(backtrack_courses) < len(self.obtain_course_name):
					cname = self.ranked_groupings_df.iloc[row, col]
					if cname in backtrack_courses:
						self.ranked_groupings_df.iloc[row, col] = None
					else:
						if cname in self.obtain_course_name:
							backtrack_courses.append(cname)

		for row in range(0, rk_mxr):
			for col in range(0, rk_mxc):
				if (self.ranked_groupings_df.iloc[row, col] in self.obtain_course_name) and (col > 0):
					col2 = col
					for edg in range(0, rk_mxc):
						if self.ranked_groupings_df.iloc[row, col2-1] is None:
							self.ranked_groupings_df.iloc[row, col2 - 1] = self.ranked_groupings_df.iloc[row, col2]
							self.ranked_groupings_df.iloc[row, col2] = None
							col2 -= 1
						else:
							break

		# print(self.ranked_groupings_df.dropna(thresh=(rk_mxc - 1), inplace=True))

		lonely_courses = []
		for row in range(0, rk_mxr):
			if self.ranked_groupings_df.iloc[row, 2] in self.obtain_course_name:
				if self.ranked_groupings_df.iloc[row, 3] is None:
					lonely_courses.append(self.ranked_groupings_df.iloc[row, 2])

		ag_mxr, ag_mxc = self.all_groupings_df.shape

		paired_courses = []
		for c1 in range(0, len(lonely_courses)):
			lcn1 = lonely_courses[c1]
			for c2 in range(c1 + 1, len(lonely_courses)):
				lcn2 = lonely_courses[c2]
				for row in range(0, ag_mxr):
					lcn1_found = False
					lcn2_found = False
					for col in range(0, ag_mxc):
						if self.all_groupings_df.iloc[row, col] == lcn1:
							lcn1_found = True
						else:
							if self.all_groupings_df.iloc[row, col] == lcn2:
								lcn2_found = True
						if lcn1_found is True and lcn2_found is True:
							paired_courses.append([lcn1, lcn2])
							break

		# print(ranked_groupings_df)
		rg_mxr, rg_mxc = self.ranked_groupings_df.shape

		for r1 in range(0, rg_mxr):
			if self.ranked_groupings_df.iloc[r1, 2] is None:
				for r2 in range(r1 + 1, rg_mxr):
					v_found = False
					if self.ranked_groupings_df.iloc[r2, 2] is not None:
						for c2 in range(2, rg_mxc):
							if self.ranked_groupings_df.iloc[r2, c2] is not None:
								self.ranked_groupings_df.iloc[r1, c2] = self.ranked_groupings_df.iloc[r2, c2]
								self.ranked_groupings_df.iloc[r2, c2] = None
								v_found = True
					if v_found is True:
						break

		self.ranked_groupings_df.reset_index(inplace=True)
		self.ranked_groupings_df.drop(columns=self.ranked_groupings_df.columns[2], inplace=True, axis=1)

		time_slots_complete = list(itertools.product(self.weekdays, self.time_slots))
		time_slots_df1 = pd.DataFrame(time_slots_complete, dtype=str)
		new_r = ['Day', 'Time Slot']
		new_r_df = pd.DataFrame(new_r).transpose()
		time_slots_df2 = pd.concat([new_r_df, time_slots_df1], ignore_index=True, axis=0)
		tt_draft_df = pd.concat([time_slots_df2, self.ranked_groupings_df], ignore_index=True, axis=1)

		tt_mxr, tt_mxc = tt_draft_df.shape
		# ap_mxr, ap_mxc = all_possible_groupings_ranked.shape

		entry_status = False
		for r in range(0, tt_mxr):
			if entry_status is False:
				center = tt_draft_df.iloc[r, 4]
				left = tt_draft_df.iloc[r, 1]
				if (pd.isna(left) is not False) and (center is not None):
					print(2)
					tt_draft_df.iloc[r, 4] = None
					for r2 in range(r - 1, 0, -1):
						for c2 in range(5, tt_mxc):
							if ((tt_draft_df.iloc[r2, c2] == '') or (tt_draft_df.iloc[r2, c2] is None) or (
									pd.isna(tt_draft_df.iloc[r2, c2]) is False) or (
									tt_draft_df.iloc[r2, c2] not in self.obtain_course_name)) and (
									entry_status is False):
								# TO DO : Collision test
								print('completed entry')
								tt_draft_df.iloc[r2, c2] = center
								entry_status = True
								break

		groupings_wb = openpyxl.Workbook()
		groupings_ws1 = groupings_wb.worksheets[0]
		groupings_ws1.title = "All Possible Combinations"
		groupings_ws2 = groupings_wb.create_sheet(title="Exclusive Combinations", index=1)
		groupings_ws3 = groupings_wb.create_sheet(title="Faculty Collisions", index=2)
		groupings_ws4 = groupings_wb.create_sheet(title="Refined Exclusive Combinations", index=3)
		groupings_ws5 = groupings_wb.create_sheet(title="Draft Timetable", index=4)

		refined_gr_df = pd.DataFrame(paired_courses)

		for r01 in dataframe_to_rows(all_possible_groupings_ranked, index=False, header=False):
			groupings_ws1.append(r01)
		for r02 in dataframe_to_rows(self.ranked_groupings_df, index=False, header=False):
			groupings_ws2.append(r02)
		for r03 in dataframe_to_rows(faculty_collisions_df, index=False, header=False):
			groupings_ws3.append(r03)
		for r04 in dataframe_to_rows(refined_gr_df, index=False, header=False):
			groupings_ws4.append(r04)
		for r05 in dataframe_to_rows(tt_draft_df, index=False, header=False):
			groupings_ws5.append(r05)

		groupings_ws1.delete_rows(1, 1)
		groupings_ws1.delete_cols(1, 1)
		# groupings_ws2.delete_rows(1, 1)
		# groupings_ws2.delete_cols(1, 2)
		# groupings_ws5.delete_cols(3, 2)

		for r in range(1, groupings_ws2.max_row + 1):
			if groupings_ws2.cell(row=r, column=1).value is None:
				groupings_ws2.delete_rows(r, 1)

		groupings_wb.save(filename="Courses Grouped.xlsx")
		
		#matrix_df_f.to_csv('matrix10classify0.csv')

		# print('Courses Grouped.xlsx is saved')
		# print('Classification Runtime : ', (time.time() - classification_start), ' seconds')
		'''

	def Generate_Timetable(self):
		self.GUI_File_Write_Status_Check('Timetable Draft 01.xlsx')

		wb = openpyxl.Workbook()
		ws = wb.worksheets[0]
		ws.title = 'Timetable'
		ws.cell(row=1, column=5).value = '01st Yr'
		# ws.cell(row=1, column=5).font = Font(color='0000CCFF')
		ws.cell(row=1, column=6).value = '02nd Yr'
		# ws.cell(row=1, column=6).font = Font(color='0000FF00')
		ws.cell(row=2, column=5).value = '03rd Yr'
		# ws.cell(row=2, column=5).font = Font(color='00660066')
		ws.cell(row=2, column=6).value = '04th Yr'
		# ws.cell(row=2, column=6).font = Font(color='00FF0000')
		# ws[str(get_column_letter(6)+str(2))].font = Font(color='00FF0000')

		ws.cell(row=1, column=6).value = 'Electives in Black'
		ws.cell(row=2, column=1).value = 'Symbiosis School for Liberal Arts'
		ws.merge_cells('A1:D1')
		ws.merge_cells('A2:D2')
		ws.cell(row=2, column=1).alignment = Alignment(horizontal='center', vertical='center')
		day = 0

		for i in range(1, 7):
			ws.cell(row=4, column=i).value = self.weekdays[day]

			ws.cell(row=5, column=i).value = self.time_slots[0]
			ws.cell(row=5, column=i).fill = PatternFill(patternType='solid', fill_type='solid',
														fgColor=Color('4F81BD'))

			ws.cell(row=20, column=i).value = self.time_slots[1]
			ws.cell(row=20, column=i).fill = PatternFill(patternType='solid', fill_type='solid',
														 fgColor=Color('92D050'))

			ws.cell(row=36, column=i).value = self.time_slots[2]
			ws.cell(row=36, column=i).fill = PatternFill(patternType='solid', fill_type='solid',
														 fgColor=Color('E46D0A'))

			ws.cell(row=51, column=i).value = self.time_slots[3]
			ws.cell(row=51, column=i).fill = PatternFill(patternType='solid', fill_type='solid',
														 fgColor=Color('CCC0DA'))

			ws.cell(row=66, column=i).value = self.time_slots[4]
			ws.cell(row=66, column=i).fill = PatternFill(patternType='solid', fill_type='solid',
														 fgColor=Color('CCC0DA'))

			day += 1

		ws.cell(row=35, column=1).value = '11:45 - 12:30 pm Lunch Break'
		ws.cell(row=35, column=1).fill = PatternFill(patternType='solid', fgColor=Color('FFFF00'))
		ws.merge_cells('A35:F35')

		for c in range(1, ws.max_column + 1):
			for r in range(1, ws.max_row + 1):
				ws.cell(row=r, column=c).border = Border(top=Side(style='thin'),
														 right=Side(style='thin'),
														 bottom=Side(style='thin'),
														 left=Side(style='thin'),
														 )
				if ws.cell(row=r, column=c).value is not None:
					ws.cell(row=r, column=c).font = Font(name='Calibri', size=11, bold=True)
					ws.cell(row=r, column=c).alignment = Alignment(horizontal='center', vertical='center')
			ws.column_dimensions[get_column_letter(c)].width = 33

		wb.save('Timetable Draft 01.xlsx')

		self.Populate_Timetable()
		# print('file generated')

	def Populate_Timetable(self):
		# mr, mc = self.ranked_groupings_df.shape
		input_df = pd.read_excel('Courses Grouped.xlsx', sheet_name='Exclusive Combinations')
		wb = openpyxl.open('Timetable Draft 01.xlsx')
		ws = wb.worksheets[0]

		max_r, max_c = input_df.shape
		in_r = 0
		in_c = 0

		for out_c in range(1, ws.max_column):
			for out_r in range(6, 81):
				if ws.cell(row=out_r, column=out_c).value not in self.time_slots:
					if (in_c < max_c) and (input_df.iloc[in_r, in_c] in self.obtain_course_name):
						ws.cell(row=out_r, column=out_c).value = input_df.iloc[in_r, in_c]
						in_c += 1
			in_r += 1

		wb.save('Timetable Draft 01.xlsx')
		output_update_Label = Tk.Label(self.window1, text="Timetable file has been generated.")
		output_update_Label.grid(row=8, column=1, padx=5, pady=5)

	@staticmethod
	def GUI_TV_Clear_Data(tv):
		tv.delete(*tv.get_children())

	@staticmethod
	def GUI_Error_File_Open():
		messagebox.showwarning("Warning", "Some output files are already open.\n"
										  "Writing to file will not be possible.\n"
										  "Please close any open files.")

	def GUI_File_Write_Status_Check(self, file_name):
		f = 'C:/' + file_name
		if os.path.exists(f):
			try:
				os.rename(f, f)
			except OSError:
				self.GUI_Error_File_Open()

	def GUI_Collision_Results(self):
		self.frame_destroy()
		results_label = Tk.Label(self.window1, text="RESULTS\nBelow are number of instances where a "
													"course/faculty/student cannot be placed in that cell.")
		student_collision_label1 = Tk.Label(self.window1, text="Number of student collisions = ")
		student_collision_label2 = Tk.Label(self.window1, text=self.collision_count_Student)
		faculty_collision_label1 = Tk.Label(self.window1, text="Number of faculty collisions = ")
		faculty_collision_label2 = Tk.Label(self.window1, text=self.collision_count_Faculty)
		course_collision_label1 = Tk.Label(self.window1, text="Number of students' common course collisions = NA")
		# course_collision_label2 = Tk.Label(self.window1, text=self.collision_count_Student_Course)
		elapsed_time_label1 = Tk.Label(self.window1, text="Time Elapsed in Seconds = ")
		elapsed_time_label2 = Tk.Label(self.window1, text=self.program_time_duration)

		results_label.grid(row=0, column=1, padx=5, pady=5)
		student_collision_label1.grid(row=1, column=1, padx=5, pady=5)
		student_collision_label2.grid(row=1, column=2, padx=5, pady=5)
		faculty_collision_label1.grid(row=2, column=1, padx=5, pady=5)
		faculty_collision_label2.grid(row=2, column=2, padx=5, pady=5)
		course_collision_label1.grid(row=3, column=1, padx=5, pady=5)
		# course_collision_label2.grid(row=3, column=2, padx=5, pady=5)

		elapsed_time_label1.grid(row=5, column=1, padx=5, pady=5)
		elapsed_time_label2.grid(row=5, column=2, padx=5, pady=5)

		next_page = Tk.Button(self.window1, text="Generate a Draft Timetable", command=self.Generate_Timetable)
		next_page.grid(row=7, column=2, padx=5, pady=5)

	def Program_GUI(self):
		# Centering the application window for the respective screen resolution.
		screen_width = self.root.winfo_screenwidth()
		screen_height = self.root.winfo_screenheight()
		app_height = 500
		app_width = 700
		x = (screen_width / 2) - (app_width / 2)
		y = (screen_height / 2) - (app_height / 2)
		self.window1.pack()
		self.root.title('Timetable')
		# self.root.iconbitmap()
		self.root.geometry(f'{app_width}x{app_height}+{int(x)}+{int(y)}')
		# About_Button = Tk.Button(self.window1, text="About", command=self.GUI_About)
		# About_Button.grid(row=0, column=0, padx=10, pady=20)
		# Help_Button = Tk.Button(self.window1, text="Help", command=self.GUI_Help)
		# Help_Button.grid(row=0, column=1, padx=10, pady=20)
		self.GUI_window1()

		self.root.resizable(True, True)
		self.root.mainloop()

	def GUI_Menu_Bar(self):
		menu_bar = Tk.Menu(self.root)
		self.root.config(menu=menu_bar)
		file_menu = Tk.Menu(menu_bar)
		edit_menu = Tk.Menu(menu_bar)
		help_menu = Tk.Menu(menu_bar)
		tools_menu = Tk.Menu(menu_bar)
		menu_bar.add_cascade(label='File', menu=file_menu)
		menu_bar.add_cascade(label='Edit', menu=edit_menu)
		menu_bar.add_cascade(label='Tools', menu=tools_menu)
		menu_bar.add_cascade(label='Help', menu=help_menu)

		file_menu.add_command(label='New', command=self.GUI_Menu_Bar_New_Command)
		edit_menu.add_command(label='Undo', command=self.GUI_placeholder)
		edit_menu.add_command(label='Redo', command=self.GUI_placeholder)
		tools_menu.add_command(label='Preferences', command=self.GUI_Preferences)
		help_menu.add_command(label='About', command=self.GUI_About)

		"""help_menu = Tk.Menu(menu_bar)
		menu_bar.add_cascade(label='Help', menu=help_menu)"""
		help_menu.add_command(label='Tutorial', command=self.GUI_Help)
		help_menu.add_separator()
		file_menu.add_command(label='Exit', command=self.root.quit)

	def GUI_Preferences(self):
		self.frame_destroy()
		self.output_all_machine_files = Tk.IntVar()
		option_output_files_bool = Tk.Checkbutton(self.window1, text="Output all dataframes as files",
												  variable=self.output_all_machine_files)
		option_output_files_bool.pack()
		output_filetype = Tk.IntVar()
		option_output_files_type = Tk.Checkbutton(self.window1, text="Output files as CSV",
												  variable=output_filetype)
		option_output_files_type.pack()
		"""
		self.theme_select = Tk.StringVar()
		theme_menu = Tk.OptionMenu(self.window1, variable=self.theme_select, *self.list_of_themes,
									command=self.GUI_Appearance)
		theme_menu.pack(expand=True)
		"""
		save_changes_button = Tk.Button(self.window1, text="Save Changes", command=self.GUI_Save_Preferences)
		save_changes_button.pack()
		back_button = Tk.Button(self.window1, text="Back", command=self.GUI_Preferences2Home)
		back_button.pack()

	"""
	def GUI_Appearance(self):
		theme_choice = self.theme_select
		gui_style = ttk.Style(self.window1)
		list_of_themes = gui_style.theme_names()
	"""

	def GUI_Preferences2Home(self):
		self.frame_destroy()
		self.GUI_window1()

	def GUI_Save_Preferences(self):
		# return [output_files_bool, output_files_type]
		self.i = 0

	def GUI_Menu_Bar_New_Command(self):
		self.frame_destroy()
		self.GUI_window1()

	def GUI_placeholder(self):
		self.i = 0
		# print('button press registered')

	@staticmethod
	def GUI_About():
		messagebox.showinfo(title='About This Program',
							message='A simple tool that assists in making of a timetable')

	@staticmethod
	def GUI_Help():
		messagebox.showinfo(title='Help',
							message='A Basic Guide to Operating the Program\n'
									'Step 1 : Click the \'Open\' button, and select the file.\n'
									'Step 2 : Check the required boxes to perform the required functions.\n'
									'\nTools available for timetable management\n'
									'Visual Lookup of Constraints : \n'
									'Move : \n'
									'Undo or Save Changes : '
							)

	def GUI_window1(self):
		label1 = Tk.Label(self.window1, text="Select File for Input")
		label1.grid(row=2, column=3, padx=5, pady=5)
		Open_Button = Tk.Button(self.window1, text="Open", command=self.GUI_window1_tasks)
		Open_Button.grid(row=2, column=5, padx=10, pady=20)
		Open_Button.configure(font=('arial', 10, 'bold'))
		TT_Validate_Button = Tk.Button(self.window1, text="Validate Timetable File",
									   command=self.File_Validation)
		TT_Validate_Button.grid(row=4, column=5, padx=10, pady=20)
		TT_Validate_Button.configure(font=('arial', 10))
		self.GUI_Menu_Bar()

	def File_Validation(self):
		filename_in = self.file_open(2)
		if filename_in != 0:
			wb = openpyxl.load_workbook(filename_in)
			ws = wb.worksheets[0]

			container_list = []
			max_row = ws.max_row
			max_col = ws.max_column

			for cell in list(ws.merged_cells):
				ws.unmerge_cells(range_string=str(cell))

			for row in range(5, max_row):
				cell = ws.cell(row=row, column=1).value
				if cell == '11:45 - 12:30 pm Lunch Break':
					for col in range(2, 7):
						ws.cell(row=row, column=col).value = '11:45 - 12:30 pm Lunch Break'

			wb.save(filename=filename_in)

			time_slots_loc = []
			day_count = 0
			df_ref_index = 0
			for col in range(1, max_col):
				day_count += 1
				for row in range(5, max_row):
					cell = ws.cell(row=row, column=col).value
					cell = self.string_validate(cell)
					if cell in self.time_slots:
						time_slots_loc.append(ws.cell(row=row, column=col).coordinate)

			for slot in range(0, len(time_slots_loc)):
				temp_store_cells = []
				if slot < (len(time_slots_loc) - 1):
					time_slot1 = time_slots_loc[slot]
					xy1 = coordinate_from_string(time_slot1)
					column_ind1 = column_index_from_string(xy1[0])
					row_ind1 = xy1[1]

					time_slot2 = time_slots_loc[slot + 1]
					xy2 = coordinate_from_string(time_slot2)
					column_ind2 = column_index_from_string(xy2[0])
					row_ind2 = xy2[1]

					if column_ind1 == column_ind2:
						upper_limit = row_ind2
					else:
						upper_limit = max_row

					for x in range(row_ind1 + 1, upper_limit):
						cell_contents = self.string_validate(ws.cell(row=x, column=column_ind1).value)
						cell_contents = cell_contents.rpartition('(')[0]
						if (cell_contents is not None) and (cell_contents != '11:45 - 12:30 pm Lunch Break'):
							temp_store_cells.append(cell_contents)

				else:
					# print('else')
					time_slot2 = time_slots_loc[slot]
					xy2 = coordinate_from_string(time_slot2)
					column_ind2 = column_index_from_string(xy2[0])
					row_ind2 = xy2[1]

					for x in range(row_ind2 + 1, max_row):
						cell_contents = self.string_validate(ws.cell(row=x, column=column_ind2).value)
						cell_contents = cell_contents.rpartition('(')[0]

						if cell_contents is None:
							# print('break at row', x)
							break
						else:
							temp_store_cells.append(cell_contents)
				container_list.append(temp_store_cells)

				# container_Df.loc[len(container_Df)] = temp_store_cells
				df_ref_index += 1

			time_slots_complete = list(itertools.product(self.weekdays, self.time_slots))
			slots_df = pd.DataFrame(time_slots_complete, dtype=str)
			container_df = pd.DataFrame(container_list, dtype=str)
			concat_df = pd.concat([slots_df, container_df], axis=1, ignore_index=True)
			concat_df.to_csv('Validated_tt.csv', encoding='utf-8-sig', index=False)

			# print('done')
			self.GUI_File_Validation_Completion()

	def GUI_File_Validation_Completion(self):
		self.frame_destroy()
		self.GUI_Menu_Bar()
		label1 = Tk.Label(self.window1, text="File Validation - Completed")
		label1.grid(row=1, column=1)

	def GUI_window1_tasks(self):
		if (self.file_open(1)) != 0:
			self.frame_destroy()
			self.GUI_window2()

	def GUI_window2(self):
		self.GUI_Menu_Bar()
		label2 = Tk.Label(self.window1, text="Pick Required Function(s)")
		label2.grid(row=3, column=1)
		self.box1_records = Tk.IntVar()
		box1_records_details = Tk.Checkbutton(self.window1, text="Generate Student Records", variable=self.box1_records)
		box1_records_details.grid(row=5, column=2)
		self.box2_TT = Tk.IntVar()
		box2_TT_details = Tk.Checkbutton(self.window1, text="Generate Time Table", variable=self.box2_TT)
		box2_TT_details.grid(row=6, column=2)
		# print("boxes", self.box1_records.get(), self.box2_TT.get())
		Confirm_Button = Tk.Button(self.window1, text="Confirm", command=self.GUI_window2_tasks)
		Confirm_Button.grid(row=7, column=2)
		Back_Button = Tk.Button(self.window1, text="Go Back", command=self.GUI_Window2_Back)
		Back_Button.grid(row=10, column=1)

	def GUI_Window2_Back(self):
		self.frame_destroy()
		self.GUI_window1()

	def GUI_window2_tasks(self):
		self.frame_destroy()
		self.GUI_Error_Screen()

	def GUI_Error_Screen(self):
		temp_label = Tk.Label(self.window1, text="ERROR")
		temp_label.grid(row=0, column=0)
		self.Excel_File_Processing()

	def GUI_window4(self):
		self.frame_destroy()
		Save_File_Button = Tk.Button(self.window1, text="Save As", command=self.file_save)
		Save_File_Button.grid(row=7, column=4)
		self.file_save()
		self.frame_destroy()

	def frame_destroy(self):
		self.window1.destroy()
		self.window1 = Tk.Frame(self.root)
		self.window1.grid()

	def file_open(self, route):
		name = filedialog.askopenfilename(initialdir="/C", title="Select Input File", filetypes=(
			("Excel Files", "*.xlsx"), ("All Files", "*.*")))
		if name != "":
			if route == 1:
				self.root_filename = name
				# , ("CSV Files", "*.csv"))
				print("filename is", self.root_filename)
			if route == 2:
				return name
		else:
			return 0

	def file_save(self):
		self.save_files_dir = filedialog.askdirectory()
		# print("dir is", self.save_files_dir)

	@staticmethod
	def file_delete(filename):
		os.remove(filename)

	# Function Description : Receive PRN, check first two numbers, return batch year
	def get_batch_year(self, prn_in):
		if len(str(prn_in)) < 11:
			# print(prn_in, 'error: incorrect length')
			pass
			# remove pass when printing.
		else:
			self.check_prn = int(str(prn_in)[:2])
			if self.check_prn == 17:
				return 2021
			if self.check_prn == 18:
				return 2022
			if self.check_prn == 19:
				return 2023
			if self.check_prn == 20:
				return 2024
			if self.check_prn == 21:
				return 2025
			if self.check_prn == 22:
				return 2026

	# Function Description : Receive string, remove formatting, then return string.
	def string_validate(self, in_str):
		self.i = 0
		return str(in_str).strip().rsplit(sep='\n', maxsplit=1)[0]


def main():
	obj = ProgramContainer()
	obj.Program_Control()
	# print('Program Runtime : ', (time.time() - start), ' seconds')


if __name__ == "__main__":
	main()
