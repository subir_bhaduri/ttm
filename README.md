# Time Table maker



## Context
This project initiated out of the need of a school to which the authors are commonly associated with. The core challenge is this: a timetable needs to be drawn up considering the following variables:
- Multiple subjects (about 90 in the current context) to be taught in a semester
- Large number of students (about 500 in our context) who chose various possible combinations of subjects as per they batch years, preferences, minor and major choices, etc. 
- Many core faculties and many visiting faculties (about 50) also have their limitations and time preferences
- Few time-table constraints implemented by the institution, such as holidays, fixed slots for subjects which are electives, etc.

## Outcome
The outcome of this project is a GUI application with following method implemented:
1) A method to enter the standard database of the subjects, respective teachers and students.
2) Creation of a standard intermediate CSV datasheet with 
    - Student names in column 1, roll numbers in column 2 and year batch as column 3.   
    - Subject names in row 1, subject code in row 2, and teacher names in row 3
3) Creation of another dataset with subject names/code in columne 1 and same sequence of subjects as entries in row 1. The cells where each subject overlaps in terms of either students, or teachers, should be marked as 1.
4) A final empty timetable GUI should be created where:
    1) The admin initiates a timeslot with mention of a particular subject, 
    2) The GUI prompts a selection of subjects which have no overlap with previous subjects in the timeslot. This is based on the database of step 3. 
    3)  Step 2 is carried out repeatedly till the number of class rooms are occpied for the timeslot, or till subjects are exhausted.

## Status
Parts figured out
- Parsing of human entered excel sheet databases of students, and subjects and teachers.
- Creation of student (rows) vs subjects (columns) database
- Creation of subject (rows) vs subjects (columen) comparison matrix

Pending:
- Removing bugs from parsing of original raw data
- Annotating and streamlining code with comments
- Creating GUI frontend application in PythonQT
- Documentation and training
