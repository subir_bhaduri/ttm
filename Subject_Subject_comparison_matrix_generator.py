import pandas as pd
import numpy as np
import re

#Student and subject matrix
df_stu_sub = pd.read_csv("matrix10classify1.csv", skiprows=1)
#print(df.head())

#get column names as list
df2 = df_stu_sub.columns#.get_values()
subjects = df2.to_list()
# remove non-subjects from the list, like 0 and PRN
del subjects[0:2]

#remove last row of sums of students
df_stu_sub.drop(df_stu_sub.index [0], inplace=True)
# remove original index column.
df_stu_sub.drop(['0'],axis=1, inplace=True)
# reset the index
df_stu_sub.reset_index(drop=True)

print(df_stu_sub.head())
print(subjects)

subject_extn = subjects.copy()
subject_extn.insert(0,'Batch')
subject_extn.insert(1,'Faculty')
subject_extn.insert(2,'Paper')

#print(subjects)

# Subject faculty dataframe
df_sub_faculty = pd.read_csv("courses_data.csv")
for idx,rows in df_sub_faculty.iterrows():
	rows['Faculty']=re.split(",|\Wand|&|/",rows['Faculty'])
	#print(rows['Faculty'])
	rows['Faculty'] = [x.strip() for x in rows['Faculty']]
	df_sub_faculty.at[idx,'Faculty_list'] = rows['Faculty']
	#print(rows['Faculty'])
print(df_sub_faculty.head())	
#print(dsds)

# create the output dataframe
df_sub_sub = pd.DataFrame(0,index=pd.Index(subjects),columns=pd.Index(subject_extn))
for subject in subjects:
	print(subject, df_sub_faculty.loc[df_sub_faculty['Course']==subject]['Batch'].values[0])
	df_sub_sub.loc[subject, 'Batch'] = df_sub_faculty.loc[df_sub_faculty['Course']==subject]['Batch'].values[0]
	df_sub_sub.loc[subject, 'Paper'] = df_sub_faculty.loc[df_sub_faculty['Course']==subject]['Paper_name'].values[0]
	df_sub_sub.loc[subject, 'Faculty'] = df_sub_faculty.loc[df_sub_faculty['Course']==subject]['Faculty'].values[0]
print(df_sub_sub.head())

#print(dsd)

df3= pd.DataFrame()
count=0

# collision detection
# for each subject compared to all other subjects, as well as faculty collisions
for subject1 in subjects:

	# Get the columns of 1/0s of the particular subject
	x = pd.Series(df_stu_sub[subject1])
	# create a set, call it the primary. It will be compared to all the rest of subjects and teachers.
	# this is a set of row indices where 1s exist.
	primary_set = set(np.where(x)[0])
	#print(subject1, primary_list)
	
	# also get facluty
	#print(list(df_sub_faculty.loc[df_sub_faculty['Course'] == subject1]['Faculty']))
	primary_faculty_set = list(df_sub_faculty.loc[df_sub_faculty['Course'] == subject1]['Faculty_list'])
	print("primary_faculty_set:",primary_faculty_set)
	#print(ddd)
	
	# Final variable of interest!
	exclusive_subjects_list = []
	
	# compare the main selected subject to all other subjects, and see if there are any overlaps
	for subject2 in subjects:
	
		# Create a secondary subset of indices of 1s
		x = pd.Series(df_stu_sub[subject2])
		secondary_set = set(np.where(x)[0])
		
		# faculty
		secondary_faculty_set = list(df_sub_faculty.loc[df_sub_faculty['Course'] == subject2]['Faculty_list'])
		
		# see if there's an "intersection" of sets
		stu_stu_overlap = primary_set & secondary_set
		
		faculty_faculty_overlap = [x for x in primary_faculty_set if x in secondary_faculty_set]
		
		#print("set:",len(y),y)
		#Check to see if there's an overlap
		if len(stu_stu_overlap) != 0 or len(faculty_faculty_overlap) != 0:
		
			# If yes, mark a 1 in the final comparison chart
			df_sub_sub.at[subject1,subject2]=1
			
			#primary_set = primary_set.union(secondary_set)
			#print("PS:",len(primary_set),primary_set)
			#exclusive_subjects_list.append(subject2)
		else:
			df_sub_sub.at[subject1,subject2]=0
			
	'''
	if len(exclusive_subjects_list) !=0:
		print("\nSub:",subject1)
		print(exclusive_subjects_list)
		print(len(exclusive_subjects_list))
		s = pd.Series(exclusive_subjects_list)
		
		#df4 = pd.DataFrame({subject1:exclusive_subjects_list})
		
		df3 = pd.concat([df3,s.rename(subject1)],axis=1)
	'''
		
# arrange the batches
df_sub_sub = df_sub_sub.sort_values(by = 'Batch')
# arrange the 1s and 0s
#for idx, row in df_sub_sub.iterrows():
	
df_sub_sub.to_csv('sub_sub_comp_matrix.csv')
		
		
			
			
	
	